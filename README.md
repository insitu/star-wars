# star-wars

This project is a Catalogue of people in Star Wars, using Google Polymer framework.

I have kept the bower_components, folder so that you can run it faster.

## Install the Polymer-CLI

First, make sure you have the [Polymer CLI](https://www.npmjs.com/package/polymer-cli) installed.

```
$ npm install -g polymer-cli
```

## Viewing the Application

```
$ polymer serve
```

The application will be available under the following URL:
http://127.0.0.1:8081

## Running Tests

```
$ polymer test
```

The application is already set up to be tested via [web-component-tester](https://github.com/Polymer/web-component-tester). Run `polymer test` to run your application's test suite locally.
